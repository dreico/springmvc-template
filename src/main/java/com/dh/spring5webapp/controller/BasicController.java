package com.dh.spring5webapp.controller;

import com.dh.spring5webapp.services.CategoryService;
import com.dh.spring5webapp.services.ItemService;
import com.dh.spring5webapp.services.SubCategoryService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BasicController {

    private ItemService service;
    private CategoryService categoryService;
    private SubCategoryService subCategoryService;

    public BasicController(ItemService service, CategoryService categoryService, SubCategoryService subCategoryService) {
        this.service = service;
        this.categoryService = categoryService;
        this.subCategoryService = subCategoryService;
    }


    @RequestMapping("/index")
    public String getIndex(Model model) {
        return "basic/index";
    }

    @RequestMapping("/products")
    public String getProducts(Model model) {
        model.addAttribute("items",service.findAll());
        model.addAttribute("categories",categoryService.findAll());
        model.addAttribute("subCategories",subCategoryService.findAll());

        return "basic/archive-blog";
    }

    @RequestMapping("/contact")
    public String getContact(Model model) {
        return "basic/contact";
    }

    @RequestMapping("/about-us")
    public String getAbout(Model model) {
        return "basic/about-us";
    }
}
